<?php
include_once "user_management.php";
//require '/kint/Kint.class.php';
//Kint::dump($_COOKIE);

if (isLoggedIn()) {
	echo "Reloaded page and still logged in";
} else if (cookieLogin()) {
	$_SESSION['username'] = $_COOKIE['username'];
	$_SESSION['password'] = $_COOKIE['password'];
	echo "Logged in with cookies";
}

if (isset($_POST['log_in'])) {
	if ($_POST['username'] != "") {
		$password = hash('sha256', $_POST['password']);
		if (login($_POST['username'], $password)) {
			$_SESSION['username'] = $_POST['username'];
			$_SESSION['password'] = $password;
			if (isset($_POST['remember']) && $_POST['remember'] == 1) {
				setcookie("username", $_POST['username'], time() + 60);
				setcookie("password", $password, time() + 60);
			}
			echo "Logged in";
		} else {
			session_destroy();
			echo "Couldn't log in, wrong username or password.";
		}
	} else {
		echo "Användarnamn saknas.";
	}
}

if (isset($_GET['logout'])) {
	logout();
}
?>

<!doctype html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Lab1</title>
        <meta name="description" content="The HTML5 Herald">
        <meta name="author" content="SitePoint">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php
		if (isLoggedIn()) {
			include_once "logged_in.php";
		} else
			include_once "login.php";
		//Måndag, den 8 Juli år 2013. Klockan är [10:59:21]
		$weekDayArray = array("Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag", "Söndag");
		$monthArray = array("Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December");
		echo $weekDayArray[date("N") - 1] . ", den " . date("j") . " " . $monthArray[date("n")] . " år ";
		echo date("Y") . ". Klockan är [" . date("H:i:s") . "]";
        ?>
    </body>
</html>